/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Mustapha
 */
public class FrontEnd extends Application {
    
    
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Parent rootResponsable = FXMLLoader.load(getClass().getResource("FXMLResponsable.fxml"));
        Scene sceneResponsable = new Scene(rootResponsable);
        Scene scene = new Scene(root);
        
        
        
        /**
         * Voila el scene deja crée w marbouta bel Interface ejdida 
         * Juste a3kes les commentaire bech tbadel mabin les interfaces fel 5edma mta3ek.
         */
        
       stage.setScene(scene);
        //stage.setScene(sceneResponsable);
        stage.setMaximized(true);
        
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
