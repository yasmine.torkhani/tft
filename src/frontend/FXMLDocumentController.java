/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontend;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import tft.dao.ArbitreDAO;
import tft.entite.Arbitre;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.WindowEvent;
/**
 *
 * @author Mustapha
 */
public class FXMLDocumentController implements Initializable {
    
    //Initialisations **********************************************************************************************
    
    /*Image agrandir = new Image("Images/Agrandir.png", true);
    Image tailleParDefaut = new Image("Images/TailleParDefaut.png", true); */
   private double initialX;
   private double initialY;
   private Button buttonAux;
   private Stage stage;
   private final String styleMenuPressed = 
                "-fx-text-alignment: left;\n" +
                "-fx-text-align: left;\n" +
                "-fx-background-color: #144d5e;"+
                "-fx-cursor : hand;"+
                "-fx-alignment : baseline-left;";
   private final String styleMenu = styleMenuPressed + "-fx-background-color: #29778e;";
   
   
   private final String styleInputError =  "-fx-border-radius: 5px;"+
    "-fx-focus-color: -fx-control-inner-background ;"+
    "-fx-faint-focus-color: -fx-control-inner-background ;"+
           " -fx-border-color: #e01212;";
   
   private final String styleInputNormal="-fx-border-color :  rgba(0,0,0,0.3);\n" +
"    -fx-border-radius: 5px;\n" +
"    -fx-focus-color: -fx-control-inner-background ;\n" +
"    -fx-faint-focus-color: -fx-control-inner-background ;";
     //*************************************************************************************************************
      private ObservableList<Arbitre> data;
      private ObservableList<Arbitre> data_filtree;
        private ObservableList<Arbitre> arbitreData ;
    @FXML
    private Label label;
    @FXML
    private Button boutonGlissantImage,boutonGlissantListe,menuCup,menuMatch,menuStade,menuMessage,menuArbitre;
    @FXML
    private Button menuMedecin;
     @FXML
    private Button menuJoueur;
    @FXML
    private Button menuFormation;
    @FXML
    private Button menuClub;
    @FXML
    private AnchorPane backgroundPane,listeContainer;
    @FXML
    private AnchorPane consulterArbitrePane;
     @FXML
    private AnchorPane consulterMedecinPane;
      @FXML
    private AnchorPane consulterJoueurPane;
       @FXML
    private AnchorPane consulterDashboardPane;
    @FXML
    private AnchorPane ajouterArbitrePane;
    @FXML
    private AnchorPane imageContainer;
    @FXML
    private ImageView optionTailleAgrandir,optionTailleParDefaut ; 
    @FXML
    private Label titelLabel;
    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private TextField cin;
    @FXML
    private TextField sexe;
    @FXML
    private TextField adresse;
    @FXML
    private PasswordField pwd;
    @FXML
    private TextField login;
    @FXML
    private ComboBox niveau;
    @FXML
    private RadioButton femme;
    @FXML
    private RadioButton homme;
    @FXML
    private TableView tableArbitre;
    
   @FXML
   TextField searchField;
    @FXML
    private TableColumn columnNom;
     
    @FXML
    private TableColumn columnPrenom;
    @FXML
    private TableColumn columnCin;
     @FXML
    private TableColumn columnAdresse;
     @FXML
    private TableColumn columnLogin;
     @FXML
    private TableColumn columnSexe;
     @FXML
    private TableColumn columnNiveau;
     @FXML
    private TableColumn columnPassword;
    
    @FXML
    private TableColumn columnButton;
    @FXML
    private TableColumn columnButtonModify;
     
    
    final ToggleGroup group = new ToggleGroup();
    private boolean CCsaisie=false;
    
    /**
     * 
     * @param o : L'objet surlequel on va faire le controle de saisie, Si = null -> tous les objets de la formulaure seront testés.
     * @return : retourner True si c'est bon.
     */
    private boolean fonctionCCsaisie(Object objetFormulaire)
    {
       if (objetFormulaire == null)
       {
        try {
             if (niveau.getValue() == null)
            {
                niveau.setStyle(styleInputError);
                throw new Exception("Niveau is null");
            }
            Integer.parseInt(cin.getText()) ;
            if ( nom.getText().isEmpty() || prenom.getText().isEmpty() || login.getText().isEmpty() || pwd.getText().isEmpty() || adresse.getText().isEmpty() || cin.getText().length() != 8) 
            {
                throw new Exception();
            }else{
            return true;    
            }
            
        }catch (Exception e)
        {
            return false;
        } 
        
       }else
       {
           
           
           switch(objetFormulaire.getClass().getName())
                   {
                       case "javafx.scene.control.TextField" :
                       TextField auxObjet = (TextField)objetFormulaire;
                            try {
                                Integer.parseInt(cin.getText()) ;
                                if (cin.getText().length() == 8)
                                   return true;
            
                                    }catch (Exception e)
                                        {
                                              return false;
                                        } 
                        /*case "javafx.scene.control.ComboBox" :
                           ComboBox auxBox = (ComboBox)objetFormulaire;
                            return auxBox.getValue() != null;
                            // A ajouter des autres Case;*/
                        default :
                                return false;
                   }
       }
    }
    
    
    @FXML
    private void isCinANumber(KeyEvent event)
    {
         
        if(fonctionCCsaisie(cin))
        {
            //cin.setStyle(styleInputNormal);
           cin.setStyle(".inputNormal");
        }else
        {
           
            cin.setStyle(styleInputError);
        }
       
        
    }
    @FXML
    private void isBoxVide(MouseEvent event)
    {
         
        if(niveau.getValue() != null)
        {
            //cin.setStyle(styleInputNormal);
           niveau.setStyle(".inputNormal");
        }else
        {
           
            niveau.setStyle(styleInputError);
        }
     
    }
      @FXML
       private void setBoxToNormal(MouseEvent event)
       {
           if(fonctionCCsaisie(niveau))
           niveau.setStyle(".inputNormal");
       }
        
    @FXML
    private void tester(MouseEvent event)
    {
        ArbitreDAO arbitredao = new ArbitreDAO();
        List<Arbitre> LArbitre =  arbitredao.getList();
        
    }
    @FXML
    private void handleInputErrors(MouseEvent event)
    {
       
    }
    
    /**
     * Interaction avec la base de données
     * 
     */
       @FXML
    private void handleAjouterArbitre(MouseEvent event)
    {
        if (fonctionCCsaisie(null))
        {
        RadioButton auxRadio = (RadioButton)group.getSelectedToggle();
          
        Arbitre arbitre = new Arbitre(nom.getText(),prenom.getText(),adresse.getText(),
                   auxRadio.getId(),cin.getText(),
                pwd.getText(),login.getText(),niveau.getValue().toString());
         
         ArbitreDAO arbitreDAO = new ArbitreDAO();
         arbitreDAO.save(arbitre);
        
   
    
    
    
    
    
         fillTable();
         
        }else
        {
             Alert alert = new Alert(Alert.AlertType.WARNING);
    alert.setTitle("Federation Tunisienne de Tennis");
    
    alert.setHeaderText("");
    alert.setContentText("Veuillez remplir tout les champs et correctement, ta3mel mzeya");
    alert.showAndWait();
            
        }
    }
    
    private Stage getStage()
    {
            return (Stage) backgroundPane.getScene().getWindow();
       
    }
  @FXML
  private void test(MouseEvent event)
  {
     
  }
    //Fonctionnement des Boutons Glissants (Liste et Images) 
    @FXML
    private void handleGlissantButtonImage( ActionEvent event)
    {
        boutonGlissantImage.setVisible(false);
        boutonGlissantListe.setVisible(true);
        imageContainer.setVisible(false);
        listeContainer.setVisible(true);
    }
    @FXML
    private void handleGlissantButtonListe( ActionEvent event)
    {
        boutonGlissantImage.setVisible(true);
        boutonGlissantListe.setVisible(false);
        
        imageContainer.setVisible(true);
        listeContainer.setVisible(false);
    }
     //*************************************************************************************************************
    
    
    private void setMenuStyleNormal()
    {
        menuJoueur.setStyle(styleMenu);
        menuArbitre.setStyle(styleMenu);
        menuMedecin.setStyle(styleMenu);
        
    }
    private void setInvisibleAllConsult()
    {
        consulterArbitrePane.setVisible(false);
        consulterMedecinPane.setVisible(false);
        consulterJoueurPane.setVisible(false);
        //consulterDashboardPane.setVisible(false);
    }
    
    @FXML
    /**
     * Juste un test
     * Encore pas affecté à un aucun objet .
     * Methode de migration d'une scene à une autre.
     */
    private void handleGo(MouseEvent event)
    {
       try {
           Parent rootResponsable = FXMLLoader.load(getClass().getResource("FXMLResponsable.fxml"));
           Scene scene = new Scene(rootResponsable);
        stage = getStage();
        
        stage.setScene(scene);
        stage.hide();
        stage.show();
           
           
       } catch (IOException ex) {
           Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    /**
     * fonctionnalité : Gerer les boutons du menu
     * @param event 
     */
    @FXML
    private void handleMenuButtonPressed( MouseEvent event)
    {
        // Configuration d'affichage des boutons
        buttonAux = (Button)event.getSource();
        setMenuStyleNormal();
        buttonAux.setStyle(styleMenuPressed);
        
        // Configuration d'affichage des Pane de gestion
        switch (buttonAux.getId())
        {
            case "menuArbitre" :
               setInvisibleAllConsult();
                consulterArbitrePane.setVisible(true);
                titelLabel.setText("Gestion des Arbitres");
                break;
            case "menuMedecin":
                setInvisibleAllConsult();
                consulterMedecinPane.setVisible(true);
                titelLabel.setText("Gestion des Medecins");
                break;
            case "menuJoueur" :
                setInvisibleAllConsult();
                consulterJoueurPane.setVisible(true);
                titelLabel.setText("Gestion des Joueurs");
                break;
                
            default:
                break;
                
        }
        
        
    }
    
    //--------------------------------------------------

    
    
    //------------------------------------------------ Configuration d'affichage genrerale -------------------------------------------------------------
     
    
    // Permet la fermeture de l'application
    @FXML
   private void handleCloseApplication(MouseEvent event)
   {
     stage = getStage();
    // do what you have to do
    stage.close();
    
     System.exit(0);
     
   }
   
    @FXML
    // Mettre la fenetre en plein ecran et Changer la source de l'icone Agrandir
    private void handleAgrandir(MouseEvent event)
    {
        //Reference vers le "Stage" de l'application crée dans "FronEnd.Java"
       stage = getStage();
       stage.setMaximized(true);
       optionTailleAgrandir.setVisible(false);
       optionTailleParDefaut.setVisible(true);    
    }
    @FXML
    //retourner la taille par defaut
    private void handleRetournerParDefaut(MouseEvent event)
    {
        //Reference vers le "Stage" de l'application crée dans "FronEnd.Java"
       stage = getStage();
       stage.setMaximized(false);
       optionTailleAgrandir.setVisible(true);
       optionTailleParDefaut.setVisible(false);
    }
     @FXML
    private void onMousePressed( MouseEvent event)
    {
        // Si l'hauteur de l'application est superieur à 800 ( Par defaut ) alors elle est en plein ecran , Cette fonctionnalité est désactivée
          stage = getStage();
          if (stage.getHeight() < 900){
         initialX = event.getX();
        initialY = event.getY();
        if(event.getButton()!=MouseButton.MIDDLE)
                {
                    initialX = event.getSceneX();
                    initialY = event.getSceneY();
                }
                else
                {
                    backgroundPane.getScene().getWindow().centerOnScreen();
                    initialX = backgroundPane.getScene().getWindow().getX();
                    initialY = backgroundPane.getScene().getWindow().getY();
                }
       // System.out.println(initialX);
          }
    }
    // Permet le deplacement de la fenetre suivant la souris
    @FXML
    private void handleonMouseDragged(MouseEvent event)
    {
        
      stage = getStage();
       
          if (stage.getHeight() < 900){
               backgroundPane.getScene().getWindow().setX(event.getScreenX() - initialX);
               backgroundPane.getScene().getWindow().setY(event.getScreenY() - initialY);
          }
                
    }
    
   //--------------------------------------------------------------------------------------------------------
    
   
   private void fillTable()
   {
         Arbitre a=new Arbitre();
           ArbitreDAO arbitreDao = new ArbitreDAO();
           columnNom.setCellValueFactory(new PropertyValueFactory("Nom"));
           columnPrenom.setCellValueFactory(new PropertyValueFactory("Prenom"));
           columnCin.setCellValueFactory(new PropertyValueFactory("Cin"));
           columnAdresse.setCellValueFactory(new PropertyValueFactory("Adresse"));
           columnLogin.setCellValueFactory(new PropertyValueFactory("Login"));
           columnPassword.setCellValueFactory(new PropertyValueFactory("pwd"));
           columnSexe.setCellValueFactory(new PropertyValueFactory("Sexe"));
           columnNiveau.setCellValueFactory(new PropertyValueFactory("Niveau"));
             
           
           Callback<TableColumn<Arbitre, String>, TableCell<Arbitre, String>> cellFactory = 
                new Callback<TableColumn<Arbitre, String>, TableCell<Arbitre, String>>()
                {
                    @Override
                    public TableCell call( final TableColumn<Arbitre, String> param )
                    {
                        final TableCell<Arbitre, String> cell = new TableCell<Arbitre, String>()
                        {

                            final Button btn = new Button( "Supprimer" );
                            @Override
                            public void updateItem( String item, boolean empty )
                            {
                                super.updateItem( item, empty );
                                if ( empty )
                                {
                                    setGraphic( null );
                                    setText( null );
                                }
                                else
                                {
                                    btn.setOnAction( ( ActionEvent event ) ->
                                            {
                                             a.setCin(getTableView().getItems().get(getIndex()).getCin()); 
                                              arbitreDao.delete(a);
                                             fillTable();
                                    } );
                                    setGraphic( btn );
                                    setText( null );
                                }
                            }
                        };
                        return cell;
                    }
                };
           
           
            Callback<TableColumn<Arbitre, String>, TableCell<Arbitre, String>> cellFactoryModify = 
                new Callback<TableColumn<Arbitre, String>, TableCell<Arbitre, String>>()
                {
                    @Override
                    public TableCell call( final TableColumn<Arbitre, String> param )
                    {
                        final TableCell<Arbitre, String> cell = new TableCell<Arbitre, String>()
                        {

                            final Button btnModify = new Button( "Modifier" );
                            @Override
                            public void updateItem( String item, boolean empty )
                            {
                                super.updateItem( item, empty );
                                if ( empty )
                                {
                                    setGraphic( null );
                                    setText( null );
                                }
                                else
                                {
                                    btnModify.setOnAction( ( ActionEvent event ) ->
                                            {
                                             a.setCin(getTableView().getItems().get(getIndex()).getCin());
                                             a.setNom(getTableView().getItems().get(getIndex()).getNom());
                                             a.setPrenom(getTableView().getItems().get(getIndex()).getPrenom());
                                             a.setLogin(getTableView().getItems().get(getIndex()).getLogin());
                                             a.setPwd(getTableView().getItems().get(getIndex()).getPwd());
                                             a.setSexe(getTableView().getItems().get(getIndex()).getSexe());
                                             a.setAdresse(getTableView().getItems().get(getIndex()).getAdresse());
                                             a.setNiveau(getTableView().getItems().get(getIndex()).getNiveau());
                                              //arbitreDao.delete(a);
                                             fillTable();
                                             FXMLModifyArbitreController.initData(a);
                                             
                                             
                                             try {
                                            Thread.sleep(1000);
                                       
                                             } catch (InterruptedException ex) {
                                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                       
                                             }
                                             //***********************************************
                                             
                                             Stage stageModify = new Stage();
                                            stageModify.setResizable(false);
                                             Parent root;
                                        try {
                                            root = FXMLLoader.load(getClass().getResource("FXMLModifyArbitre.fxml"));
                                            stageModify.setScene(new Scene(root));
                                        } catch (IOException ex) {
                                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                            
                                             stageModify.setTitle("Modifier un Arbitre");
                                                     stageModify.initModality(Modality.WINDOW_MODAL);
                                            stageModify.initOwner(
                                               ((Button)event.getSource()).getScene().getWindow() );
                                             stageModify.setOnHidden((WindowEvent e)->{
                                             fillTable() ;});
                                                     
                                                     
                                                     stageModify.show();
                                             
                                    } );
                                    setGraphic( btnModify );
                                    setText( null );
                                }
                            }
                        };
                        return cell;
                    }
                }; 
            
            columnButtonModify.setCellFactory(cellFactoryModify);
            columnButton.setCellFactory( cellFactory );
            data = FXCollections.observableArrayList();
               for (Arbitre aIterator : arbitreDao.getList())
               {
                   data.add(aIterator);
               }
            
            tableArbitre.setItems(data);
            tableArbitre.setEditable(true);
        
        
            
   }
   @FXML
   
   public void rechercher(KeyEvent event)
   {
       Arbitre a=new Arbitre();
           ArbitreDAO arbitreDao = new ArbitreDAO();
           columnNom.setCellValueFactory(new PropertyValueFactory("Nom"));
           columnPrenom.setCellValueFactory(new PropertyValueFactory("Prenom"));
           columnCin.setCellValueFactory(new PropertyValueFactory("Cin"));
           columnAdresse.setCellValueFactory(new PropertyValueFactory("Adresse"));
           columnLogin.setCellValueFactory(new PropertyValueFactory("Login"));
           columnPassword.setCellValueFactory(new PropertyValueFactory("pwd"));
           columnSexe.setCellValueFactory(new PropertyValueFactory("Sexe"));
           columnNiveau.setCellValueFactory(new PropertyValueFactory("Niveau"));
             
           
           Callback<TableColumn<Arbitre, String>, TableCell<Arbitre, String>> cellFactory = 
                new Callback<TableColumn<Arbitre, String>, TableCell<Arbitre, String>>()
                {
                    @Override
                    public TableCell call( final TableColumn<Arbitre, String> param )
                    {
                        final TableCell<Arbitre, String> cell = new TableCell<Arbitre, String>()
                        {

                            final Button btn = new Button( "Supprimer" );
                            @Override
                            public void updateItem( String item, boolean empty )
                            {
                                super.updateItem( item, empty );
                                if ( empty )
                                {
                                    setGraphic( null );
                                    setText( null );
                                }
                                else
                                {
                                    btn.setOnAction( ( ActionEvent event ) ->
                                            {
                                             a.setCin(getTableView().getItems().get(getIndex()).getCin()); 
                                              arbitreDao.delete(a);
                                             fillTable();
                                    } );
                                    setGraphic( btn );
                                    setText( null );
                                }
                            }
                        };
                        return cell;
                    }
                };
           
           
            Callback<TableColumn<Arbitre, String>, TableCell<Arbitre, String>> cellFactoryModify = 
                new Callback<TableColumn<Arbitre, String>, TableCell<Arbitre, String>>()
                {
                    @Override
                    public TableCell call( final TableColumn<Arbitre, String> param )
                    {
                        final TableCell<Arbitre, String> cell = new TableCell<Arbitre, String>()
                        {

                            final Button btnModify = new Button( "Modifier" );
                            @Override
                            public void updateItem( String item, boolean empty )
                            {
                                super.updateItem( item, empty );
                                if ( empty )
                                {
                                    setGraphic( null );
                                    setText( null );
                                }
                                else
                                {
                                    btnModify.setOnAction( ( ActionEvent event ) ->
                                            {
                                             a.setCin(getTableView().getItems().get(getIndex()).getCin());
                                             a.setNom(getTableView().getItems().get(getIndex()).getNom());
                                             a.setPrenom(getTableView().getItems().get(getIndex()).getPrenom());
                                             a.setLogin(getTableView().getItems().get(getIndex()).getLogin());
                                             a.setPwd(getTableView().getItems().get(getIndex()).getPwd());
                                             a.setSexe(getTableView().getItems().get(getIndex()).getSexe());
                                             a.setAdresse(getTableView().getItems().get(getIndex()).getAdresse());
                                             a.setNiveau(getTableView().getItems().get(getIndex()).getNiveau());
                                              //arbitreDao.delete(a);
                                             fillTable();
                                             FXMLModifyArbitreController.initData(a);
                                             
                                             /**
                                              *  Attendre pour que les donneés soit envoyées
                                              */
                                             try {
                                            Thread.sleep(1000);
                                       
                                             } catch (InterruptedException ex) {
                                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                       
                                             }
                                             //***********************************************
                                             
                                             Stage stageModify = new Stage();
                                            stageModify.setResizable(false);
                                             Parent root;
                                        try {
                                            root = FXMLLoader.load(getClass().getResource("FXMLModifyArbitre.fxml"));
                                            stageModify.setScene(new Scene(root));
                                        } catch (IOException ex) {
                                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                            
                                             stageModify.setTitle("Modifier un Arbitre");
                                                     stageModify.initModality(Modality.WINDOW_MODAL);
                                            stageModify.initOwner(
                                               ((Button)event.getSource()).getScene().getWindow() );
                                             stageModify.setOnHidden((WindowEvent e)->{
                                             fillTable() ;});
                                                     
                                                     
                                                     stageModify.show();
                                             
                                    } );
                                    setGraphic( btnModify );
                                    setText( null );
                                }
                            }
                        };
                        return cell;
                    }
                };
            columnButtonModify.setCellFactory(cellFactoryModify);
            columnButton.setCellFactory( cellFactory );
           data_filtree = FXCollections.observableArrayList();
           arbitreDao.getList().stream().filter((aIterator) -> ( aIterator.getNom().contains(searchField.getText()) ||  aIterator.getPrenom().contains(searchField.getText()) )).forEach((aIterator) -> {
               data_filtree.add(aIterator);
       });
            
            tableArbitre.setItems(data_filtree);
            tableArbitre.setEditable(true);
        
        
            
   }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
       niveau.getItems().addAll("Amateur","National","International");
       femme.setToggleGroup(group);
       homme.setSelected(true);
       homme.setToggleGroup(group);
       fillTable();
       
       
    }    
    
}
