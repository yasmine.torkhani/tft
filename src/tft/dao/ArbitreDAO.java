/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tft.dao;
import static java.lang.String.format;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import tft.config.DBconnexion;
import tft.entite.Arbitre;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 *
 * @author Mustapha
 */
public class ArbitreDAO implements ICrudDAO<Arbitre>{
    
    private Connection connexion;
    private Statement ste;
    private PreparedStatement pre;
     public ArbitreDAO()
    {
        try {
            connexion = DBconnexion.getConnexion();
            ste = connexion.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(ArbitreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save(Arbitre t) {
         try {
           String req1 = "insert into personne (nom,prenom,adresse,sexe,login,pwd,role,niveau,cin) VALUES (?,?,?,?,?,?,?,?,?) ";
           pre = connexion.prepareStatement(req1);     
           
           pre.setString(1, t.getNom());
           pre.setString(2, t.getPrenom());
           pre.setString(3, t.getAdresse());
           pre.setString(4, t.getSexe());
           pre.setString(5, t.getLogin());
           pre.setString(6, t.getPwd());
           pre.setString(7,"Arbitre");
           pre.setString(8, t.getNiveau());
           pre.setString(9, t.getCin());
           
           pre.execute();
              Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Federation Tunisienne de Tennis");
    
    alert.setHeaderText("");
    alert.setContentText("L'arbitre a été ajouté avec succés");
    alert.showAndWait();
        } catch (SQLException ex) {
        }
    }

    @Override
    public void delete(Arbitre t) {
         LocalDateTime timePoint = LocalDateTime.now(); 
         String format = "dd/MM/yyyy";
         System.out.println(timePoint.format(DateTimeFormatter.ISO_DATE));
         java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format ); 
       try {
            String req= "update personne set date_destruction =  '" + timePoint.format(DateTimeFormatter.ISO_DATE) +"' where cin ='"+t.getCin()+"' and role='Arbitre'" ; 
                    
                    //"
                    //+ " where cin="+t.getCin()+" and role= 'Arbitre';";
            ste.executeUpdate(req);
        } catch (SQLException ex) {
         System.out.println(ex);
        }
    }

    @Override
    public List<Arbitre> getList() {
        List<Arbitre> LArbitre = new ArrayList<>();
        Arbitre arbitre ;
       
        String req= "select * from personne where role = 'Arbitre' and date_destruction is null";
        try {
            ResultSet res =  ste.executeQuery(req);
            //public Arbitre(String nom, String prenom, String adresse, String sexe, int cin, String pwd, String login, String niveau) 
            while (res.next()) {
               arbitre= new Arbitre(res.getString("nom"),
                       res.getString("prenom"),
                        res.getString("adresse"),
                res.getString("sexe"),
                       res.getString("cin"),
                       res.getString("pwd"),
                       res.getString("login"),
                       res.getString("niveau"));
               LArbitre.add(arbitre);
            }
            
        } catch (SQLException ex) {
            
        }
        return LArbitre;
    }

    @Override
    public void update(Arbitre t) {
        
        try {
            String req= "update personne set nom = '" + t.getNom()+"', prenom ='"
                    +t.getPrenom()+"', adresse = '"+t.getAdresse()+"',login='"+t.getLogin()+"',pwd='"+t.getPwd()+"'"+",niveau='"+t.getNiveau()+"'  where cin = '"+t.getCin()+"'";
                    
                    //"
                    //+ " where cin="+t.getCin()+" and role= 'Arbitre';";
            ste.executeUpdate(req);
        } catch (SQLException ex) {
         System.out.println(ex);
        }
        
    }
    
     
}
