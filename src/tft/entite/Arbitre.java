/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tft.entite;


/**
 *
 * @author Mustapha
 */
public class Arbitre extends Personne{
   
    private String cin;
    private String pwd;
    private String login;
    private String niveau;
public Arbitre()
{
    super();
}

    public void setCin(String cin) {
        this.cin = cin;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }
    public Arbitre(String nom, String prenom, String adresse, String sexe, String cin, String pwd, String login, String niveau) {
     super(nom,prenom,adresse,sexe);
        this.cin = cin;
        this.pwd = pwd;
        this.login = login;
        this.niveau = niveau;
    }

   

    public String getCin() {
        return cin;
    }

    public String getPwd() {
        return pwd;
    }

    public String getLogin() {
        return login;
    }

    public String getNiveau() {
        return niveau;
    }

    
    
}
